-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 03 Mai 2022 à 14:05
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bd_proj_integrateur`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `idClient` int(11) NOT NULL,
  `nomClient` varchar(100) NOT NULL,
  `prenomClient` varchar(300) NOT NULL,
  `sexeClient` varchar(2) NOT NULL,
  `dateNaissanceClient` date NOT NULL,
  `emailClient` varchar(100) NOT NULL,
  `adressePostaleClient` varchar(100) NOT NULL,
  `typeUtilisateur` varchar(100) NOT NULL,
  `motDePasseClient` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`idClient`, `nomClient`, `prenomClient`, `sexeClient`, `dateNaissanceClient`, `emailClient`, `adressePostaleClient`, `typeUtilisateur`, `motDePasseClient`) VALUES
(2, 'LLoris', 'Hugo', 'M', '1995-02-14', 'hugo@gmail.com', '18 rue pontrique 64100 Bayonne', 'Utilisateur', '$2y$10$HMYVTCWQMnM66v1Ms1dnle.06VIrv4nVsgqrkMDIVjVGg9w702xm2'),
(3, 'Tampo', 'Ruth', 'F', '1999-02-27', 'judith@gmail.com', '4 rue port vieux 64600 Biarritz', 'Utilisateur', '$2y$10$/vkcAODRKxyJDuc4aNmqjuYEY6D.QW7JJxoMw3TmWNsPwe5.cBQOS'),
(4, 'Linch', 'Becky', 'F', '1989-05-07', 'becky@gmail.com', '17 rue pannecqueau 64100 Bayonne', 'Utilisateur', '$2y$10$3uAS0s7X/BHqnlz8mbbLu.hFS3vehsMChZvCd9.DAFnOh83dNeviC'),
(5, 'Hugues', 'Matt', 'M', '1992-03-10', 'matt@gmail.com', '12 rue de la mairie 64100 Bayonne', 'Utilisateur', '$2y$10$216e1/TGL52n3YovAYoDWO2OVdDZqYpSNJiz1mJGUa0A9IbWaGPJG'),
(6, 'baba', 'claude', 'M', '1998-07-14', 'iot@gmail.com', '18 rue pontrique 64100 Bayonne', 'Utilisateur', '$2y$10$FC6401pf0SEpKUBsLWgBv.HwoFnT8cXAbM4ATaN8QVFyxq1YY9qH2'),
(7, 'Namajunas', 'Rose', 'F', '1995-08-13', 'namajunas@gmail.com', '4 rue belle vue 64100 Bayonne', 'Utilisateur', '$2y$10$oYRTPH4QitTP6VmnVFD0u.p7KaqFH/171NEFUbVGrBZu7km/h581y'),
(8, 'Lebanner', 'Gérard', 'M', '1996-11-10', 'lebanner@gmail.com', '14 rue de milan 64600 Anglet', 'Administrateur', '$2y$10$PybMXzse9Kr0gwPbaXK3G.Ani5PnuVQPUAZ1xOgFp932KXv1HKtWK'),
(9, 'Bouleau', 'Laure', 'F', '1995-07-14', 'laure@gmail.com', '18 rue de Lisses 64100 Bayonne', 'Utilisateur', '$2y$10$dkTTPKm0n9oXFPffg.ahSuSyd23vatU9tuyC9/FU1vviXjQkmGnQC'),
(10, 'AKOUN', 'Bienvenu', 'M', '1999-01-01', 'bienvenu@gmail.com', '5 Traversée de l\'Adour 64100 Bayonne', 'Administrateur', '$2y$10$hjNHB1FJfTn4u4PEYgvsaeQOU09JOVYBjWRG8fQ0nxXo0l.T/5P56'),
(13, 'Roman', 'Reigns', 'M', '1987-02-18', 'reigns@gmail.com', '10 Rue de la Cathédrale 64600 Anglet', 'Utilisateur', '$2y$10$88I0XwsQRz.QZW5IVUDxg.gVBbSmiP48/T/HpizY9hBejkBwLaFzS'),
(14, 'Banks', 'Sasha', 'F', '1984-01-13', 'sasha@gmail.com', '65 Rue Villeneuve 64100 Bayonne', 'Utilisateur', '$2y$10$lU75bQe.yO7yHGkohLg9wuUVFl2DmXFUTBUfGpsrXr4FPNiz2mAcO'),
(15, 'Wendy', 'Renard', 'F', '1990-09-07', 'wendy@gmail.com', '27 Rue de la Liberté 64600 Anglet', 'Utilisateur', '$2y$10$MD7Wryubmzg5E3x7gYUNYO1gHWMuXXA38jDAHzQQfl31pOXn7YSvu'),
(16, 'Renard', 'Hervé', 'M', '1989-05-21', 'herverenard@gmail.com', '45 rue Mobibois 64600 Anglet', 'Utilisateur', '$2y$10$3.CMBODquu./T4rhE3r1yewJ9iHzKNVH.ZuSXDe3YL5Ec0f5Mh3Cy'),
(17, 'Seth', 'Rollins', 'M', '1995-07-10', 'seth@gmail.com', '18 rue de la Joie 6100 Bayonne', 'Utilisateur', '$2y$10$NrLytNz7uLribIkkIMK7nOvBulf52smwwBs0oxvZhu/lnP9oV.R56'),
(18, 'Shevchenko', 'Valentina', 'F', '1993-06-18', 'valentina@gmail.com', '78 Avenue Allées Marines 64100 Bayonne', 'Utilisateur', '$2y$10$kjejumjIO9J1z7WEEHigIORou/eijM4XjVR9qpTP2d/nk4AkdnXB6'),
(19, 'Nunes', 'Amanda', 'F', '1985-02-22', 'nunes@gmail.com', '23 Allée Platanes 64100 Bayonne', 'Utilisateur', '$2y$10$hlS.ANsK9cbHFvp5gfi80e6mQhw6GzPdIp.PzlIxvAYw5VCxGujw6'),
(20, 'Zhang', 'Welhi', 'F', '1988-12-13', 'zhang@gmail.com', '86 Avenue Allées Paulmy 64100 Bayonne', 'Utilisateur', '$2y$10$sEiHRDAfLeH9lcX9rhx6Su.QfKfaqemaQIKHZ..1mHKyHcC1cIKp6'),
(21, 'Lohoues', 'Emma', 'F', '1991-06-18', 'lohoues@gmail.com', '45 rue Amedee Dufourg 64600 Anglet', 'Utilisateur', '$2y$10$DSNf5o7mwsSPfvydube7XeeVUIcuxYJnpbrpAfQzO..yJTKQnOV8S'),
(22, 'Coco', 'Emilia', 'F', '1987-03-29', 'emilia@gmail.com', '32 rue Albert le Barillier 64600 Anglet', 'Utilisateur', '$2y$10$Kn78z3Rx.VJ9mF9boFKYL.n4T7N/ZzxJ9NASRR3/.HCvnv3VjuFvy'),
(23, 'Baker', 'Joséphine', 'F', '1985-09-13', 'josephine@gmail.com', '27 rue Alberte Maite 64600 Anglet', 'Utilisateur', '$2y$10$4JUFPgZJ763vDYmoUlggrOJrWClRD71m8kHH1HKUIVzOJ/sD.dFJa'),
(24, 'Esparza', 'Carla', 'F', '1997-12-08', 'carla@gmail.com', '5 rue André Brousse 64600 Anglet', 'Utilisateur', '$2y$10$i.EL8WsTgcWY5roRVePqne.FmzyieRfgFwQXBShIbzFJy8qmnxpr6'),
(25, 'Fiorot', 'Manon', 'F', '1994-12-02', 'manon@gmail.com', '17 rue Appolon 64600 Anglet', 'Utilisateur', '$2y$10$oSEzRGFzhHxSbINuj3lo8uNxJwrSV/YlYDJpFPuXrKZEeBlyxB39S'),
(26, 'Andrade', 'Jessica', 'F', '1993-08-14', 'jessica2@gmail.com', '26 rue Auguste Guimont 64600 Anglet', 'Utilisateur', '$2y$10$/.VKQQb0ms40OF1d1hA2/ewKzll31Nr9Uy4UZM6CRNs.JV3JImDmu'),
(27, 'Lopez', 'Claire', 'F', '2000-02-10', 'claire1@gmail.com', '32 rue Calliope 64600 Anglet', 'Utilisateur', '$2y$10$macrm/6POzuIoojzXug8peNnWJQcD09T0gio.WBjdu.3C9iBk7q42'),
(28, 'Bertaud', 'Lucie', 'F', '2002-11-18', 'bertaud@gmail.com', '36 rue Camille Clement 64600 Anglet', 'Utilisateur', '$2y$10$SfqdIVI7yhztlCHXRg.l4ODoU.E7yG0Deiwmf65cPNpPR7FWh5oMO'),
(29, 'Kholi', 'Virat', 'M', '1997-01-18', 'virat@gmail.com', '78 rue Aguerria 64700 Hendaye', 'Utilisateur', '$2y$10$mpFtf66ohQ2mv/jxggctIOEmLG.7zzRHI7hxXjm.BNPsTxDuv1UBO'),
(30, 'Murty', 'Sudha', 'F', '2000-07-13', 'murty@gmail.com', '3 rue Arrieta 64500 Saint-Jean de Luz', 'Utilisateur', '$2y$10$L4PQD9pBrCIg1h2JX9Sev.Z3.g6HhasaayOJ1xl8t/lXePhtfxIEK'),
(31, 'Pope', 'Francis', 'M', '2001-04-18', 'pope@gmail.com', '45 rue Aiche Egina 64700 Hendaye', 'Utilisateur', '$2y$10$ppxO0YkU0ZDRRhrL0LqawuJSiZbBya39Z3duNoyrutdo06yPQHlDG'),
(32, 'Liyuan', 'Pend', 'F', '2001-03-18', 'penf@gmail.com', '12 rue de la Providence 64500 Saint-Jean de Luz', 'Utilisateur', '$2y$10$y8CLj/aEp/vpAP53Nl1UK.zHR17tAOgFOCn8nPBefF2XslB.8cBaa'),
(33, 'Ma', 'Jack', 'M', '2002-02-27', 'jackma@gmail.com', '56 rue Aizpurdy 64700 Hendaye', 'Utilisateur', '$2y$10$xAADY74Z7SJiCetV2ZX6.uU620/xZQCLd1HeHeNhc1wT.1EL6TVUS'),
(34, 'Swift', 'Taylor', 'F', '1999-07-05', 'swifttaylor@gmail.com', '89 rue Garat 40180 Dax', 'Utilisateur', '$2y$10$cal.hG6aRnUjan7CEfBz.OYOv2n/xf.JypdAvJu8ZBcYrXCNtU78m'),
(35, 'Widodo', 'Jocko', 'M', '1996-08-13', 'jocko@gmail.com', '23 rue de Lohitzun 40390 Saint-Martin de Seignanx', 'Utilisateur', '$2y$10$Uj14L9DTZwhRPXI4z8x0Quwo14ZGoFq7v.49dbh5yBwjaMZbk7yLa'),
(36, 'Winfrey', 'Oprah', 'F', '1995-01-13', 'oprah@gmail.com', '19 rue de la Rhune 64500 Saint-Jean de Luz', 'Utilisateur', '$2y$10$ykQRPZ84w0vjAny4gruZPO5DuYnzpB972suFh9E0xt48MzMtGTDyW'),
(37, 'Lama', 'Dalai', 'M', '1996-11-03', 'dalai@gmail.com', '12 rue Gaetan 64200 Biarritz', 'Utilisateur', '$2y$10$hlMPl6hqxmI.BaXqU75JD.Ipfw3h4iPweINNydjRGdXXXzTQVZWVm'),
(38, 'Thunberg', 'Greta', 'F', '1994-07-29', 'greta@gmail.com', '73 rue Aroka 64990 Mouguerre', 'Utilisateur', '$2y$10$YiNhG8mSEYM4p/xNXembTO4eJVQU.uRS4KCzG70Xrvtm/7BVY2E7K'),
(39, 'Jury', 'Jury', 'F', '1999-02-27', 'jury@gmail.com', 'jury', 'Utilisateur', '$2y$10$0mGElbJ0eoezuYiNMK2O9eyIEUOGXZ9.IFXUhA8brI5l63i8w1bGW');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `numeroCommande` int(11) NOT NULL,
  `dateCommande` date NOT NULL,
  `idClient` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `commandecontientproduit`
--

CREATE TABLE `commandecontientproduit` (
  `numeroCommande` int(11) NOT NULL,
  `idProduit` int(11) NOT NULL,
  `quantiteCommandee` int(11) NOT NULL,
  `tailleCommandee` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `idProduit` int(11) NOT NULL,
  `lienProduit` varchar(500) NOT NULL,
  `descriptionProduit` varchar(200) NOT NULL,
  `quantiteEnStock` int(11) NOT NULL,
  `prixProduit` double NOT NULL,
  `idType` int(11) NOT NULL,
  `tailleVetement` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`idProduit`, `lienProduit`, `descriptionProduit`, `quantiteEnStock`, `prixProduit`, `idType`, `tailleVetement`) VALUES
(1, 'https://fr.louisvuitton.com/images/is/image/lv/1/PP_VP_L/louis-vuitton-sac-od%C3%A9on-mm-toile-monogram-sacs-%C3%A0-main--M45355_PM2_Front%20view.jpg', 'Sac à main Louis Vuitton', 4, 35.99, 2, '---'),
(2, 'https://www.collectorsquare.com/images/products/372465/00pp-sac-a-main-celine-luggage-en-python-vert.jpg', 'Sac à main vert', 3, 25.99, 2, '---'),
(3, 'https://www.cdiscount.com/pdt2/6/5/5/1/700x700/cit8416092411655/rw/sac-a-main-femme-en-cuir-pu-bandoulieres-sacs-port.jpg', 'Sac à main rose', 2, 35.99, 2, '---'),
(4, 'https://www.dhresource.com/0x0/f2/albu/g10/M01/2A/B1/rBVaWVxQF0SAG8mEAABXPghLzII371.jpg/women-dresses-satin-classic-ladies-evening.jpg', 'Robe rouge de soirée', 4, 25.99, 1, 'S'),
(5, 'https://www.cdiscount.com/pdt2/2/1/3/1/700x700/auc2009468168213/rw/sac-a-main-femme-mode-pu-cuir-plaid-classique-sac.jpg', 'Sac à main rose clair', 4, 35.99, 2, '---'),
(6, 'https://www.cdiscount.com/pdt2/7/3/1/1/700x700/pom0791265034731/rw/pomelo-best-sac-a-main-femme-en-pu-cuir-design-en.jpg', 'Sac à main noir & rouge', 5, 15.99, 2, '---'),
(7, 'https://www.cdiscount.com/pdt2/6/9/4/1/700x700/cus2009797379694/rw/sac-a-main-femme-gris-sac-a-bandouliere-en-pu-cuir.jpg', 'Sac à main gris', 7, 20.99, 2, '---'),
(8, 'https://www.cdiscount.com/pdt2/9/6/2/1/700x700/auc6288911194962/rw/qianle-sac-a-main-femme-avec-noeud-de-papillon-sac.jpg', 'Sac à main kaki & noir', 1, 30.99, 2, '---'),
(9, 'https://www.cdiscount.com/pdt2/9/7/6/1/700x700/sty4055755027976/rw/stylebreaker-sac-de-bowling-avec-motif-usa-tete-d.jpg', 'Sac à main noir avec tête de mort', 4, 18.99, 2, '---'),
(10, 'https://www.cdiscount.com/pdt2/2/7/9/1/700x700/ban3701425424279/rw/sac-a-main-maplesage-noir-a-motif-vintage-rouge-c.jpg', 'Sac à main noir avec motifs', 3, 25.99, 2, '---'),
(11, 'https://www.cdiscount.com/pdt2/4/4/7/1/700x700/afj2009407223447/rw/sac-marque-sac-a-main-de-marque-pour-femme-sac-cab.jpg', 'Sac à main rouge avec fleur', 6, 15.99, 2, '---'),
(12, 'https://www.cdiscount.com/pdt2/3/6/1/1/700x700/afj2009770896361/rw/sac-a-main-femme-nouvelle-mode-sac-a-main-de-luxe.jpg', 'Sac à main rouge Louis Vuitton', 4, 40.99, 2, '---'),
(13, 'https://www.cdiscount.com/pdt2/5/7/6/1/700x700/pom0602430304576/rw/sac-a-main-femme-sac-bandouliere-femme-cabas-sac-d.jpg', 'Sac à main 3 couleurs', 1, 35.99, 2, '---'),
(14, 'https://www.cdiscount.com/pdt2/4/9/1/1/700x700/auc2009869475491/rw/sweet-sa-femmes-pu-cuir-sac-a-main-bleu-ciel.jpg', 'Sac à main bleu & ciel', 4, 17.99, 2, '---'),
(15, 'https://www.cdiscount.com/pdt2/7/0/7/1/700x700/afs2009486506707/rw/sac-a-main-femme-nouvelle-arrivee-sac-a-main-de-ma.jpg', 'Sac à main bleu foncé', 2, 35.99, 2, '---'),
(16, 'https://www.cdiscount.com/pdt2/7/7/7/1/700x700/afj2009793003777/rw/sac-a-main-femme-de-marque-sac-a-bandouliere-femme.jpg', 'Sac à main noir avec nounours', 4, 21.99, 2, '---'),
(18, 'https://robe-vintage.com/wp-content/uploads/2021/04/Robe-annee-60-soiree-bleu-min.jpg', 'Robe bleue à points blancs', 4, 35.99, 1, 'M'),
(19, 'https://cdn.shopify.com/s/files/1/0529/1671/5679/products/HTB1thlLXL1G3KVjSZFkq6yK4XXai_baf8cfae-f924-45ba-bd92-caf5d99e8bdb.jpg?v=1615662801', 'Robe blanche à carreaux rouges', 4, 30.99, 1, 'S'),
(20, 'http://cdn.shopify.com/s/files/1/0191/2297/8882/products/robe-vintagebr-annee-50-rouge-779304_1200x1200.jpg', 'Robe rouge avec points blancs', 6, 27.99, 1, 'XS'),
(22, 'https://robesapois.fr/wp-content/uploads/2020/09/14_200004891_A004_5_100014064.jpg', 'Robe 4', 4, 25.99, 1, 'XL'),
(23, 'https://www.cockpit-boutique.com/1984-large_default/robe-polka-marron-pois-blanc.jpg', 'Robe 5', 4, 33.99, 1, 'L'),
(25, 'https://ae01.alicdn.com/kf/HTB1drt0RVXXXXb4aXXXq6xXFXXXa/Polka-Dot-Dress-Rockabilly-Dresses-Robe-Femme-2019-Strapless-50s-60s-Robe-Vintage-Vestidos-Pin-Up.jpg', 'Robe 6', 4, 25.99, 1, 'XXL'),
(26, 'https://ae01.alicdn.com/kf/HTB1fQPDRVXXXXbxXXXXq6xXFXXXp/Robe-r-tro-fleurs-style-Rockabilly-pour-femmes-Vintage-ann-es-60-1950-robes-d-t.jpg', 'Robe 7', 4, 20.99, 1, 'S'),
(28, 'http://cdn.shopify.com/s/files/1/0347/9204/6730/products/robe-rockabilly-rouge-et-noir___9_800x.jpg?v=1602270818', 'Robe 8', 4, 17.99, 1, 'L'),
(29, 'https://www.laboutiqueduhauttalon.fr/blog/wp-content/uploads/2016/06/robe-polka-noire-a-pois-bleus-1.jpg', 'Robe 9', 4, 23.99, 1, 'M'),
(30, 'https://robe-vintage.com/wp-content/uploads/2021/04/Robe-annee50-a-pois-bleu-min.jpg', 'Robe 10', 4, 35.99, 1, 'XS'),
(31, 'http://cdn.shopify.com/s/files/1/0191/2297/8882/products/robe-vintagebr-1950-bleu-cerises-877188_1200x1200.jpg?v=1612443941', 'Robe 11', 4, 25.99, 1, 'XXL'),
(32, 'http://cdn.shopify.com/s/files/1/0278/5637/1782/products/Robe-Vintage-Motif-Noel_ccc2ecd5-82c9-4bc2-be11-374ca03d91eb.jpg', 'Robe 12', 4, 45.99, 1, 'S'),
(33, 'https://ae01.alicdn.com/kf/Hfa81c1ead015406e87131ce41b1b0a9dX.jpg', 'Robe 13', 4, 20.99, 1, 'L'),
(34, 'http://cdn.shopify.com/s/files/1/0191/2297/8882/products/robe-vintage-verte-annee-50-416413_1200x1200.jpg?v=1620250488', 'Robe 14', 4, 35.99, 1, 'XXL'),
(35, 'https://cdn.shopify.com/s/files/1/0191/2297/8882/products/robe-vintagebr-annee-50-225950_1024x1024.jpg?v=1612444010', 'Robe 15', 4, 15.99, 1, 'XS'),
(53, 'https://www.cdiscount.com/pdt2/6/9/4/1/700x700/cus2009797379694/rw/sac-a-main-femme-gris-sac-a-bandouliere-en-pu-cuir.jpg', 'SAC A MAIN TEST', 3, 40.99, 2, '---'),
(54, 'https://cdn.shopify.com/s/files/1/0319/3543/0796/products/robe-vintage-pour-ado-365.jpg', 'Robe verte et orange', 3, 33.99, 1, 'M'),
(55, 'https://cdn.shopify.com/s/files/1/0345/9753/5875/products/robe-vintage-classe-571_1024x1024.jpg?v=1634391662', 'Robe bleue avec papillon', 3, 17.99, 1, 'L'),
(56, 'https://www.dhresource.com/0x0/f2/albu/g10/M00/26/DF/rBVaWV2UOfSARfv8AAWWcvTKEiI780.jpg', 'Robe rouge à points', 2, 10.99, 1, 'L'),
(57, 'https://cdn.shopify.com/s/files/1/0329/3944/2220/products/robe-pin-up-rockabilly-fleuris___39_2000x.jpg?v=1587548625', 'Robe à fleurs', 3, 35.99, 1, 'S');

-- --------------------------------------------------------

--
-- Structure de la table `typeproduit`
--

CREATE TABLE `typeproduit` (
  `idType` int(11) NOT NULL,
  `typeProduit` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `typeproduit`
--

INSERT INTO `typeproduit` (`idType`, `typeProduit`) VALUES
(1, 'Vêtement'),
(2, 'Sac à main');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`idClient`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`numeroCommande`);

--
-- Index pour la table `commandecontientproduit`
--
ALTER TABLE `commandecontientproduit`
  ADD PRIMARY KEY (`idProduit`,`numeroCommande`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`idProduit`);

--
-- Index pour la table `typeproduit`
--
ALTER TABLE `typeproduit`
  ADD PRIMARY KEY (`idType`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `idClient` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `numeroCommande` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `idProduit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT pour la table `typeproduit`
--
ALTER TABLE `typeproduit`
  MODIFY `idType` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
