<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./../style.css" />
</head>
<body>

<?php


if(isset($_POST['submit'])){

	// Connexion à la bd
    require('./../connexionbd.php');


  // récupérer les données saisies par l'utilisateur
  
  $nom = $_POST['nom'];
  $prenom = $_POST['prenom'];
  $sexe = $_POST['sexe'];
  $dateNaissance = $_POST['dateNaissance'];
  $email = $_POST['email'];
  $adressePostale = $_POST['adressePostale'];
  $typeUtilisateur = $_POST['typeUtilisateur'];
  $motDePasse = password_hash($_POST['motDePasse'], PASSWORD_DEFAULT);
  
  
  
  	// Vérification dans la bd si le mail existe déjà
  	
$stmt = $access->prepare("SELECT * FROM client WHERE emailClient=?");
$stmt->execute([$email]); 
$client = $stmt->fetch();

if ($client) {  // Si le mail existe déjà alors impossible

	echo "<div class='sucess'>
             <h3>Adresse mail non disponible</h3>
             <p>Cliquez ici pour <a href='ajouterUtilisateur.php'>recommencer</a></p>
       </div>";	
}

else {  

   // Requête mysql pour insérer des données dans la base de données
  
  $sql = "INSERT into client(`nomClient`, `prenomClient`,`sexeClient`,`dateNaissanceClient`,
  `emailClient`,`adressePostaleClient`,`typeUtilisateur`,`motDePasseClient`) 
  VALUES (:nom,:prenom,:sexe,:dateNaissance,:email,:adressePostale,:typeUtilisateur,:motDePasse)";

  $res = $access->prepare($sql);
  $exec = $res->execute(array(":nom"=>$nom,":prenom"=>$prenom,":sexe"=>$sexe,":dateNaissance"=>$dateNaissance,
  ":email"=>$email,":adressePostale"=>$adressePostale,":typeUtilisateur"=>$typeUtilisateur,":motDePasse"=>$motDePasse));
  
  // vérifier si la requête d'insertion a réussi
  if($exec){
    echo "<div class='sucess'>
             <h3>L'utilisateur a été ajouté avec succès!!!</h3>
             <p>Cliquez ici pour revenir à <a href='accueilAdmin.php'>votre espace administrateur</a></p>
       </div>";	
  }
  else{
    echo "<div class='sucess'>
             <h3>Adresse mail non disponible</h3>
             <p>Cliquez ici pour vous <a href='ajouterUtilisateur.php'>recommencer</a></p>
       </div>";  }
}
}


else{
?>
	<! Formulaire d'inscription >

<form class="box" action="" method="post">
      <h1 class="box-title">AJOUT D'UTILISATEUR</h1>
   
    <! Champ pour le nom >
    <h4 align="center"> Nom * :  
    <input type="text" class="box-input" name="nom" 
     placeholder="" required /> </h4>
    
     <! Champ pour le prénnom >
    <h4 align="center"> Prénom * :  
    <input type="text" class="box-input" name="prenom" 
     placeholder="" required /> </h4>
    
    <! Champ pour le sexe >
   <h4 align="center" > Sexe * : </h4> 
   <h4 align="center"><input type="radio" name="sexe"value="F" /> F   &nbsp;&nbsp;
   <input type="radio"name="sexe"value="M" />  M </h4>
  
    <! Champ pour la date de naissance>
    <h4 align="center"> Date de naissance * : 
    <input type="date" class="box-input" name="dateNaissance" 
     placeholder="jj/mm/aaaa" required /> </h4>

    <! Champ pour le mail>
     <h4 align="center"> Email * : 
     <input type="email" class="box-input" name="email" 
     placeholder="" required /> </h4>
     
    <! Champ pour l'adresse postale>
    <h4 align="center"> Adresse postale * :  
    <input type="text" class="box-input" name="adressePostale" 
     placeholder="" required /> </h4>
    
    <! Champ pour le type d'utilisateur>
   <h4 align="center" > Type d'utilisateur * : </br> 
   <input type="radio" name="typeUtilisateur"value="Administrateur" /> Administrateur du site  &nbsp;&nbsp;
   <input type="radio"name="typeUtilisateur"value="Utilisateur" />  Utilisateur classique </h4>

       
     <! Champ pour le mot de passe>
     <h4 align="center" > Mot de passe * :
    <input type="password" class="box-input" name="motDePasse" 
  placeholder="" required /> </h4> 
  
  <! Bouton de validation >
    <input type="submit" name="submit" 
  value="Ajouter l'utilisateur" class="box-button" />
  
</form>
<?php } ?>
</body>
</html>
