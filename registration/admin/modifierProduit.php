<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./../style.css" />
</head>
<body>

<?php


if(isset($_POST['submit'])){

	// Connexion à la bd
    require('./../connexionbd.php');


  // récupérer les données saisies par l'utilisateur
  
  $idProduitRecup = $_REQUEST['idProduit'];
  $descriptionProduit = $_REQUEST['descriptionProduit'];
  $quantiteEnStock = $_REQUEST['quantiteEnStock'];
  $prixProduit = $_REQUEST['prixProduit'];
  $typeProduit = $_REQUEST['typeProduit'];
  $tailleVetement = $_REQUEST['tailleVetement'];
 
  // Vérification dans la bd si l' id existe 
  	  	
$stmt = $access->prepare("SELECT * FROM produit WHERE idProduit=?");
$stmt->execute([$idProduitRecup]); 
$VerifProduit = $stmt->fetch();

if ($VerifProduit) {  // Si l'id existe dans la bd alors 

// Requete de mise à jour 			
$ordreSQL = "UPDATE produit
		  SET descriptionProduit='$descriptionProduit', 
		      quantiteEnStock = '$quantiteEnStock' , 
			prixProduit='$prixProduit', 
			idType='$typeProduit',
			tailleVetement='$tailleVetement'
		  WHERE idProduit=$idProduitRecup " ;
// Exécution de la requête avec la méthode exec et récupération du nombre de tuples modifié
$nb = $access -> exec($ordreSQL) ;
// Vérification
if($nb != 0)
{  
    echo "<div class='sucess'>
             <h3>Les données ont été modifiées avec succès!!!</h3>
             <p>Cliquez ici pour revenir à <a href='accueilAdmin.php'>votre espace administrateur</a></p>
       </div>";	
         	
  }
  		
else {
    echo "<div class='sucess'>
             <h3>Echec de modification des données de ce produit dans la base de données!!</h3>
             <p>Cliquez ici pour <a href='modifierProduit.php'>recommencer</a></p>
       </div>";  
    }
}


else {  

	echo "<div class='sucess'>
             <h3>ID non disponible dans la base de donnée</h3>
             <p>Cliquez ici pour <a href='modifierProduit.php'>recommencer</a></p>
       </div>";
}
}

else{
?>
	<! Formulaire d'ajout >

<form class="box" action="" method="post">
      <h1 class="box-title">MODIFIER PRODUIT</h1>
      
   
     <!-- Champ pour l'id du produit -->
     <h3 align="center"> ID du produit * : 
     <input type="text" class="box-input" name="idProduit" 
     placeholder="" required /> </h3>
	 
     <! Champ pour la description du produit >
    <h4 align="center"> Description du produit * :  
    <input type="text" class="box-input" name="descriptionProduit" 
     placeholder="" required /> </h4>
    
    <! Champ pour la quantité en stock > 
    <h4 align="center"> Quantité en stock * :
    <input type="text" class="box-input" name="quantiteEnStock" 
     placeholder="" required /> </h4>
     
    <! Champ pour le prix du produit >
    <h4 align="center"> Prix * : 
    <input type="text" class="box-input" name="prixProduit" 
     placeholder="" required /> </h4>
        
    <! Champ pour le type de produit >
   <h4 align="center" > Type du produit * : </h4> 
   <h4 align="center"><input type="radio" name="typeProduit"value="1" /> Vêtement  &nbsp;&nbsp; 
   <input type="radio"name="typeProduit"value="2" />  Sac à main </h4>
   
       <! Champ pour la taille si c'est un vêtement>
 <div>
   <h4 align="center" > Si c'est un vêtement, choisir sa taille  :    
   		<select name="tailleVetement">
							<option value="XS"> XS </option>   
							<option value="S"> S </option>  							 
							<option value="M"> M </option> 
							<option value="L"> L </option>
							<option value="XL"> XL </option>
							<option value="XXL"> XXL </option>
							<option value="---"selected="selected"> --- </option>
		</select>        
   </h4>            
</div>

                         
  <! Bouton de validation >
    <input type="submit" name="submit" 
  value="ENREGISTRER" class="box-button" />
  
</form>
<?php } ?>
</body>
</html>
