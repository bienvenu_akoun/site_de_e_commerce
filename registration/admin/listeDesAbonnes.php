<?php

	require("./../connexionbd.php") ;

?>

<!doctype html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">

    <title> PROJET INTEGRATEUR L2 INFO </title>
        
    <!-- Custom CSS -->
    <link rel="stylesheet" href="./../style1.css">
	   
    </head>
  
  
<body> </br> </br>
	<fieldset> 
				<legend align="center"> LISTE DES ABONNE(E)S ET STATISTIQUES</legend> </br>  

	<table class="Stat">
								<caption> STATISTIQUES SUR LES ABONNES</caption>
								<thead><tr> 
								<th>Nombre total d'abonnés</th> <th>Nombre d'administrateurs</th> <th>Nombre d'abonnés classiques </th>
								<th>Nombre de femmes </th> <th>Nombre d'hommes</th>  
								</thead></tr> 
								
								<tbody> 
											<! Corps ou contenu du tableau >								
								
	
<?php 
// Création des requêtes
$ordreSQL1= " SELECT COUNT(idClient) AS 'Total1' FROM client  " ;
$ordreSQL2= " SELECT COUNT(idClient) AS 'Total2' FROM client WHERE typeUtilisateur LIKE 'Administrateur'  " ;
$ordreSQL3= " SELECT COUNT(idClient) AS 'Total3' FROM client WHERE typeUtilisateur LIKE 'Utilisateur'  " ;
$ordreSQL4= " SELECT COUNT(idClient) AS 'Total4' FROM client WHERE sexeClient LIKE 'F'  " ;
$ordreSQL5= " SELECT COUNT(idClient) AS 'Total5' FROM client WHERE sexeClient LIKE 'M'  " ;
// Exécution des requêtes 
$LaRequeteExecutee1=$access->query($ordreSQL1) ;
$LaRequeteExecutee2=$access->query($ordreSQL2) ;
$LaRequeteExecutee3=$access->query($ordreSQL3) ;
$LaRequeteExecutee4=$access->query($ordreSQL4) ;
$LaRequeteExecutee5=$access->query($ordreSQL5) ;

// Récupération des resultats renvoyés 
$leTuple1=$LaRequeteExecutee1->fetch() ;
$leTuple2=$LaRequeteExecutee2->fetch() ;
$leTuple3=$LaRequeteExecutee3->fetch() ;
$leTuple4=$LaRequeteExecutee4->fetch() ;
$leTuple5=$LaRequeteExecutee5->fetch() ;
 ?>	
 
 	<tr>
	<td> <?php echo $leTuple1['Total1'] ; ?> </td>
	<td> <?php echo $leTuple2['Total2'] ; ?> </td>
	<td> <?php echo $leTuple3['Total3'] ; ?> </td>
	<td> <?php echo $leTuple4['Total4'] ; ?> </td>
	<td> <?php echo $leTuple5['Total5'] ; ?> </td>
 	
								</tbody>
	</table> </br>

				
				<table class="PosTableau">
				<caption> LISTE DE TOUS LES ABONNES</caption>
							<thead><tr>  
										<th>ID</th> <th>NOM</th>  <th>PRENOM</th> <th>SEXE</th> <th>DATE DE NAISSANCE</th> <th>EMAIL</th> <th>ADRESSE</th> <th>TYPE D'UTILISATEUR</th>
							</tr></thead>
									<tbody> 
											<! Corps ou contenu du tableau >
<?php
// Création de la requête
$ordreSQL= " SELECT * FROM client ORDER BY typeUtilisateur ASC " ;

// Exécution de la requête 
$LaRequeteExecutee=$access->query($ordreSQL) ;

// Récupération du resultat renvoyé par la requête dans un tableau contenant tous les tuples
$lesTuples=$LaRequeteExecutee->fetchall() ;

foreach($lesTuples as $leTuple){ 
?>
	<tr>
	<td> <?php echo $leTuple["idClient"] ; ?> </td>
	<td> <?php echo $leTuple["nomClient"] ; ?> </td>
	<td> <?php echo $leTuple["prenomClient"] ; ?> </td>
	<td> <?php echo $leTuple["sexeClient"] ; ?> </td>
	<td> <?php echo $leTuple["dateNaissanceClient"] ; ?> </td>
	<td> <?php echo $leTuple["emailClient"] ; ?> </td>
	<td> <?php echo $leTuple["adressePostaleClient"] ; ?> </td>
	<td> <?php echo $leTuple["typeUtilisateur"] ; ?> </td>
	</tr>
<?php } ?>													
				</tbody> 
				</table> </br>
				
				
	</fieldset> 

</body>
</html> 