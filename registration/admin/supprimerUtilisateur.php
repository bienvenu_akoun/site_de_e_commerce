<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./../style.css" />
</head>
<body>

<?php


if(isset($_POST['submit'])){

	// Connexion à la bd
    require('./../connexionbd.php');


  // récupérer les données saisies par l'utilisateur
  
  $email = $_POST['email'];
    
 // Vérification dans la bd si le mail existe 
  	  	
$stmt = $access->prepare("SELECT * FROM client WHERE emailClient=?");
$stmt->execute([$email]); 
$client = $stmt->fetch();

if ($client) {  // Si le mail existe dans la bd alors suppression

  $sql = "DELETE FROM client WHERE emailClient=?";

 $res = $access->prepare($sql);
 $exec = $res->execute(array($email)) ;
    
  // vérifier si la requête d'insertion a réussi
  if($exec){
    echo "<div class='sucess'>
             <h3>L'utilisateur a été supprimé avec succès!!!</h3>
             <p>Cliquez ici pour revenir à <a href='accueilAdmin.php'>votre espace administrateur</a></p>
       </div>";	
  }
  else{
    echo "<div class='sucess'>
             <h3>Adresse mail non disponible</h3>
             <p>Cliquez ici pour vous <a href='ajouterUtilisateur.php'>recommencer</a></p>
       </div>";  }
	
}

else {  

	echo "<div class='sucess'>
             <h3>Adresse mail non disponible</h3>
             <p>Cliquez ici pour <a href='ajouterUtilisateur.php'>recommencer</a></p>
       </div>";
}
}


else{
?>
	<! Formulaire de suppression >

     <form class="box" action="" method="post">
      <h1 class="box-title">SUPPRESSION D'UTILISATEUR</h1>
   

    <! Champ pour le mail>
     <h3 align="center"> Email de l'utilisateur * : 
     <input type="email" class="box-input" name="email" 
     placeholder="" required /> </h3>
     
	<! Bouton de validation >  
    <input type="submit" name="submit" 
  value="Supprimer l'utilisateur" class="box-button" />
  
</form>
<?php } ?>
</body>
</html>

