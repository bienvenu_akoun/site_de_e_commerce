<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./../style.css" />
</head>
<body>

<?php


if(isset($_POST['submit'])){

	// Connexion à la bd
    require('./../connexionbd.php');


  // récupérer les données saisies par l'utilisateur
  
  $idProduitRecup = $_REQUEST['idProduit'];
    
 // Vérification dans la bd si l' id existe 
  	  	
$stmt = $access->prepare("SELECT * FROM produit WHERE idProduit=?");
$stmt->execute([$idProduitRecup]); 
$VerifProduit = $stmt->fetch();

if ($VerifProduit) {  // Si l'id existe dans la bd alors suppression

  $sql = "DELETE FROM produit WHERE idProduit=?";

 $res = $access->prepare($sql);
 $exec = $res->execute(array($idProduitRecup)) ;
    
  // vérifier si la requête d'insertion a réussi
  if($exec){
    echo "<div class='sucess'>
             <h3>Le produit a été supprimé avec succès!!!</h3>
             <p>Cliquez ici pour revenir à <a href='accueilAdmin.php'>votre espace administrateur</a></p>
       </div>";	
  }
  else{
    echo "<div class='sucess'>
             <h3>Echec de suppression du produit</h3>
             <p>Cliquez ici pour vous <a href='supprimerProduit.php'>recommencer</a></p>
       </div>";  }
	
}

else {  

	echo "<div class='sucess'>
             <h3>ID non disponible dans la base de donnée</h3>
             <p>Cliquez ici pour <a href='supprimerProduit.php'>recommencer</a></p>
       </div>";
}
}


else{
?>
	<!-- Formulaire de suppression -->

     <form class="box" action="" method="post">
      <h1 class="box-title">SUPPRESSION DE PRODUIT</h1>
   

    <!-- Champ pour l'id du produit -->
     <h3 align="center"> ID du produit * : 
     <input type="number" class="box-input" name="idProduit" 
     placeholder="" required /> </h3>
     
	<! Bouton de validation >  
    <input type="submit" name="submit" 
  value="Supprimer le produit" class="box-button" />
  
</form>
<?php } ?>
</body>
</html>

