<?php

	require("./../connexionbd.php") ;

?>

<!doctype html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">

    <title> PROJET INTEGRATEUR L2 INFO </title>
        
    <!-- Custom CSS -->
    <link rel="stylesheet" href="./../style1.css">
	   
    </head>
  
  
<body> </br> </br>
	<fieldset> 
				<legend align="center"> LISTE DES PRODUITS ET STATISTIQUES</legend> </br> 

	<table class="Stat">
								<caption> STATISTIQUES SUR LES PRODUITS</caption>
								<thead><tr> 
								<th>Nombre total de produits</th> <th>Nombre de vêtements</th> <th>Nombre de sacs à mains </th>  
								</thead></tr> 
								
								<tbody> 
											<! Corps ou contenu du tableau >								
								
	
<?php 
// Création des requêtes
$ordreSQL1= " SELECT COUNT(idProduit) AS 'Total1' FROM produit  " ;
$ordreSQL2= " SELECT COUNT(idProduit) AS 'Total2' FROM produit WHERE idType = 1  " ;
$ordreSQL3= " SELECT COUNT(idProduit) AS 'Total3'  FROM produit WHERE idType = 2  " ;

// Exécution des requêtes 
$LaRequeteExecutee1=$access->query($ordreSQL1) ;
$LaRequeteExecutee2=$access->query($ordreSQL2) ;
$LaRequeteExecutee3=$access->query($ordreSQL3) ;

// Récupération des resultats renvoyés 
$leTuple1=$LaRequeteExecutee1->fetch() ;
$leTuple2=$LaRequeteExecutee2->fetch() ;
$leTuple3=$LaRequeteExecutee3->fetch() ;

 ?>	
 
 	<tr>
	<td> <?php echo $leTuple1['Total1'] ; ?> </td>
	<td> <?php echo $leTuple2['Total2'] ; ?> </td>
	<td> <?php echo $leTuple3['Total3'] ; ?> </td>
 	
								</tbody>
	</table> </br></br>
				
				<table class="PosTableau">
				<caption>LISTE DE TOUS LES PRODUITS</caption>
							<thead><tr>  
										<th>ID</th> <th>IMAGE DU PRODUIT</th> <th>LIEN INTERNET</th>  <th>DESCRIPTION</th> <th>QUANTITE</th> <th>PRIX</th> <th>TYPE</th> <th>TAILLE</th>
							</tr></thead>
									<tbody> 
											<! Corps ou contenu du tableau >
<?php
// Création de la requête
$ordreSQL= " SELECT * FROM produit ORDER BY idType ASC " ;

// Exécution de la requête 
$LaRequeteExecutee=$access->query($ordreSQL) ;

// Récupération du resultat renvoyé par la requête dans un tableau contenant tous les tuples
$lesTuples=$LaRequeteExecutee->fetchall() ;

foreach($lesTuples as $leTuple){ 
?>
	<tr>
	<td> <?php echo $leTuple["idProduit"] ; ?> </td>  
	<td> <img src="<?php echo $leTuple["lienProduit"] ; ?> " alt="logo" " width="40" height="60"/> </td>
	<td> <a> <?php echo $leTuple["lienProduit"] ; ?>  </a> </td>
	<td> <?php echo $leTuple["descriptionProduit"] ; ?> </td>
	<td> <?php echo $leTuple["quantiteEnStock"] ; ?> </td>
	<td> <?php echo $leTuple["prixProduit"] ; ?> € </td>
	<td> <?php echo $leTuple["idType"] ; ?> </td>
	<td> <?php echo $leTuple["tailleVetement"] ; ?> </td>

	</tr>
<?php } ?>													


				</tbody> 
				</table> </br>

				
				
				
	</fieldset>

</body>
</html> 
