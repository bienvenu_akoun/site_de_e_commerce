<?php
	
	// Fonction d'ajout de produits à la bd
 
   	function ajouterProduit($lienProduit, $descriptionProduit, $quantiteEnStock, $prixProduit)
   	{
   		if(require("connexionbd.php")) // Si connexion réussie avec la bd alors :
   		{
   				// req et prepare : variables créées
   			
   			$req = $access ->prepare("INSERT INTO produit (lienProduit, descriptionProduit, quantiteEnStock, prixProduit) VALUES ('$lienProduit', '$descriptionProduit', $quantiteEnStock, $prixProduit)");   			
   				// Exécution de la requête
   			$req->execute(array($lienProduit, $descriptionProduit, $quantiteEnStock, $prixProduit)) ;
   			
   			$req->closeCursor() ;   // Pour fermer tout
   		}
   	}
   	
   	// Fonction d'affichage de tous les produits de la bd
   	
   	function affichertousLesProduit()
   	{
   		if(require("connexionbd.php")) // Si connexion réussie avec la bd alors :
			{
				// req et prepare : variables créées		
				$req=$access->prepare("SELECT * FROM produit ORDER BY idProduit DESC") ;
				
				// Exécution de la requête
   			      $req->execute() ;
   			      
   			      // Récupération des données de la requête en PDO; data : variable choisie
   			      $data = $req->fetchAll(PDO::FETCH_OBJ) ;	
   			      
   			      return $data ;	
   			      
   			      $req->closeCursor() ;  // Pour fermer tout	
			}   	
   	}
   	
   	
   	// Fonction d'affichage des vetements
   	
   	function afficherVetements()
   	{
   		if(require("connexionbd.php")) // Si connexion réussie avec la bd alors :
			{
				// req et prepare : variables créées		
				$req=$access->prepare("SELECT * FROM produit Where idtype=1 ORDER BY idProduit DESC") ;
				
				// Exécution de la requête
   			      $req->execute() ;
   			      
   			      // Récupération des données de la requête en PDO; data : variable choisie
   			      $data = $req->fetchAll(PDO::FETCH_OBJ) ;	
   			      
   			      return $data ;	
   			      
   			      $req->closeCursor() ;  // Pour fermer tout	
			}   	
   	}

   	
   	// Fonction d'affichage des sacs à main
   	
   	function afficherSac()
   	{
   		if(require("connexionbd.php")) // Si connexion réussie avec la bd alors :
			{
				// req et prepare : variables créées		
				$req=$access->prepare("SELECT * FROM produit Where idtype=2 ORDER BY idProduit DESC") ;
				
				// Exécution de la requête
   			      $req->execute() ;
   			      
   			      // Récupération des données de la requête en PDO; data : variable choisie
   			      $data = $req->fetchAll(PDO::FETCH_OBJ) ;	
   			      
   			      return $data ;	
   			      
   			      $req->closeCursor() ;  // Pour fermer tout	
			}   	
   	}
   	
   /*	
   	// Fonction pour récupérer le nom et prénom de l'utlisateur connecté
   	
   	function nomPrenomUser()
   	{
   		if(require("connexionbd.php")) // Si connexion réussie avec la bd alors :
			{
				require('login.php');
				$emailrecup=$_POST['email'] ;
				// req et prepare : variables créées		
				$req=$access->prepare("SELECT nomClient FROM client Where emailClient LIKE '$emailrecup' ") ;
				
				// Exécution de la requête
   			      $req->execute() ;
   			      
   			      // Récupération des données de la requête en PDO; data : variable choisie
   			      $data = $req->fetchAll(PDO::FETCH_OBJ) ;	
   			      
   			      return $data ;	
   			      
   			      $req->closeCursor() ;  // Pour fermer tout	
			}   	
   	}

  */ 	
   	
   	
   	
   	
   	
   	

   	
   	// Fonction pour supprimer produits
   	
   	function supprimerProduit($idProduit)
   	{
   		if(require("connexionbd.php")) // Si connexion réussie avec la bd alors :
			{
				// req et prepare : variables créées		
				$req=$access->prepare("DELETE FROM produit WHERE idProduit = ? ") ;
						
				// Exécution de la requête
   			      $req->execute(array($idProduit)) ;		
			}
   	
   	
   	
   	}
   	

?>