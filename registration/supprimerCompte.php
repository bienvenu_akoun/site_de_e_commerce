<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="./style.css" />
</head>
<body>

<?php


if(isset($_REQUEST['submit'])){

if (isset($_REQUEST['email']) && isset($_REQUEST['motDePasse'])
	&& isset($_REQUEST['confSuppression']))
{
	
	// Connexion à la bd
    require('connexionbd.php');


  // récupérer les données saisies par l'utilisateur
  
  $email = $_REQUEST['email'];
  $motDePasse = password_hash($_REQUEST['motDePasse'], PASSWORD_DEFAULT);
  $confirmation = $_REQUEST['confSuppression'] ;

// Si l'utilisateur a confirmé la suppression du compte alors :	
if($confirmation == "choixOui")
{
	// On vérifie le mail dans la bd  	
	$stmt = $access->prepare("SELECT * FROM client WHERE emailClient=?");
	$stmt->execute([$email]); 
	$client = $stmt->fetch();
	
	if ($client) 
	{
		// On vérifie le mot de passe
			if ($client && password_verify($_REQUEST['motDePasse'], $client['motDePasseClient']))
			{ 
				// Requete de suppression 			
				$ordreSQL = "DELETE FROM client Where emailClient LIKE '$email' " ;
				// Exécution de la requête avec la méthode exec et récupération du nombre de tuples modifié
				$nb = $access -> exec($ordreSQL) ;
				// Vérification
					if($nb != 0) 
					{
							// Initialiser la session
							session_start();
							
							// Détruire la session.
							if(session_destroy())
							{
								// Redirection vers la page d'accueil du site
								header("Location: ./../Fich HTML/AccueilSite.html");
							}
					}
					else 
					{
						echo "<div class='sucess'>
							<h3>Echec de suppression du compte </h3>
							</div>";			
					}
							
			}
			else 
			{
				echo "<div class='sucess'>
						<h3>Mot de passe incorrect </h3>
					</div>";
			}		
	}
	else 
	{
		 	echo "<div class='sucess'>
				<h3>Adresse mail incorrecte </h3>
            </div>";
	}
 



}

// Si confirmation de suppression non effectuée, rien ne se passe 
else 
{
	echo "<div class='sucess'>
			<h3>Compte non supprimé : vous n'avez pas confirmé ce choix!</h3>
		  </div>";

} 


}
  	  	
} // Fin du if(isset($_REQUEST['submit']))
 
else{
?>
	<! Formulaire de suppression du compte >

     <form class="box" action="" method="post">
      <h1 class="box-title">SUPPRIMER SON COMPTE</h1>
   

    <! Champ pour le mail>
     <h3 align="center"> Adresse mail * : 
     <input type="email" class="box-input" name="email" 
     placeholder="" required /> </h3>
               
    <! Champ pour le mot de passe >
     <h3 align="center" > Mot de passe * :</h3> 
    <input type="password" class="box-input" name="motDePasse" 
  placeholder="" required />

	<! Bouton radio pour confirmer ou non la suppression du compte >
	<h4 align="center" > Êtes-vous sûr(e)s de vouloir supprimer le compte * :</h4> 
	<h4 align="center"><input type="radio"name="confSuppression"value="choixOui" />  OUI  &nbsp;&nbsp;
	<input type="radio"name="confSuppression"value="choixNon" checked="checked" /> NON 	</h4>
     
	<! Bouton de validation >  
    <input type="submit" name="submit" 
  value="Supprimer le compte" class="box-button" />
  
</form>
<?php } ?>
</body>
</html>





