<?php

	session_start();  // démarrage d'une session

// on vérifie que les variables de session identifiant l'utilisateur existent
if ($_SESSION["authenOK"] == true) 
{
	require("./fichierFonction.php") ;
	$mesvetementsbd=afficherVetements() ;  // mesvetementsbd : variable créée pour recevoir les vetements de la bd 
	$mesacsbd=afficherSac() ;  // mesacsbd : variable créée pour recevoir les vetements de la bd 

?>

<!doctype html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">

    <title> PROJET INTEGRATEUR L2 INFO </title>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="./css/bootstrap.min.css">
        
    <!-- Custom CSS -->
    <link rel="stylesheet" href="./style1.css">
	<!-- Box icons( site for icones ) -->
	<link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
	   
    </head>
  
  
<body>  

<header>
		 <div class="nav container"> 

												<!-- Option du menu -->

                            <li>
                            	<select name="Choix" onChange="location.href=''+this.options[this.selectedIndex].value+'.php';">  
						<option value="#" align="center">LES PRODUITS EN VENTE</option>                                                                                 	
						<option value="./vetementAbonne">Les vêtements </option>
						<option value="#">Les sacs à main</option> 
						<option value="./accueilAbonne">Tous les produits</option>                          	                            	
                            	</select>
                            </li>

                            <li> 
						<select name="Choix" onChange="location.href=''+this.options[this.selectedIndex].value+'.php';"> 
						<option value="#" align="center">GERER SON COMPTE</option>
						<option value="./changerMotDePasse">Changer son mot de passe </option>
						<option value="./changerEmail">Changer son email</option>
						<option value="./supprimerCompte">Supprimer son compte</option>
						<option value="./logout">Déconnexion</option>
						</select>                                                                             
                            </li> 
						
						 <li> |||  </li> 
								<!-- Image du panier d'achat --> 
					   <i class='bx bxs-shopping-bag' id="cart-icon" >	</i> 
		
			<!-- Onglet du contenu du panier -->
							
			<div class="cart">
			<h2 class="cart-title"> Contenu de votre panier d'achat </h2>
			
			<!-- cart content -->
				<div class="cart-content">
				
				</div>
				
				<!--total-->
				<div class="total">
					<div class="total-title">Total</div>
					<div class="total-price">0€</div>
				</div>
				<!--buy Button/boutton d'achat-->
				<button type="button" class="btn-buy"> Terminer l'achat </button>
				<!-- cart close-->
				<i class='bx bx-x' id="close-cart"></i>	                                                                       
				
		</div>     
		
		</div>	<!-- Fin de nav container -->
 </header>
  
  </br> </br> 
   
<!-- <div id="affichageProduit"> -->


<section class="shop container">
	
	<div id="GrosTitreSac"> 
    <! Texte pour type de produits : SACS A MAIN >   
	<p class="section-title"> NOS SACS A MAIN </p>
	</div> </br> </br>
	
	<! Affichage des sacs >
	<div class="shop-content">
	
	<?php foreach($mesacsbd as $produit): ?> <!-- Bloc php pour la répétition zone d'affichage du produit -->
		
		<div class="product-box">
			<div class="shadowbox"> 
			
			<img src=" <?= $produit->lienProduit ?>" alt="" class="product-img" >    <!-- Lien du produit -->
			
						
		   <h5 class="product-title" align="center"><?= substr($produit->descriptionProduit, 0, 200) ?> </h5>  <!-- Description du produit limité à 200 caractères avec substr -->	
		   
		   <h5 align="center" class="quantite"> Quantité disponible : <?= $produit->quantiteEnStock ?></h5> </br> <!-- prix du produit -->
		   
			<span class="price"> <?= $produit->prixProduit ?> € </span> <!-- prix du produit -->
			 	
			<i class='bx bx-shopping-bag add-cart'> </i>	</br> </br>	 <!-- Bouton d'ajout au panier 	-->			 
		
			</div> <!-- Fin shadowbox -->
		</div>  <!-- Fin product-box -->
	
	<?php endforeach ; ?>  <!-- Fin du Bloc php pour la répétition zone d'affichage du produit --> 
	
	</div> </br> </br> <!-- Fin shop-content -->

	


	
</section>
		
		
			<!-- Lien vers le fichier js-->
	<script src="./main.js"></script>
		
</body>
</html>

<?php
}

else { 
	// Sinon message d'erreur et l'utilisateur est invité à se connecter
    echo "<div class='sucess'>
				Veuillez vous connecter 
       </div>";	  
}



?>

